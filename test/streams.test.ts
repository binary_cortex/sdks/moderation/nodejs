import Tap from 'tap';
import TOS from '../lib/consts/tos.enum';
import {
	ActionResult,
	AppealStreams,
	ArrayEvent, ContentStreams, CreatorStreams, MapEvent, TaskAction, TextEvent, WebhookStreamLike, WebhookStreams,
} from '../lib';
import { randomBytes, randomUUID } from 'crypto';
import WebhookEvent, { AppealResult, DecisionTypes } from '../lib/objects/webhook.obj';

const spamTaskAction: TaskAction = {
	id: randomUUID(),
	name: 'Spamming',
	description: 'spam',
	image: '',
	childIds: [],
	organizationId: randomUUID(),
	root: false,
	slug: TOS.Spamming,
	actionResult: ActionResult.REMOVAL,
	appealable: true,
	accountAction: 0,
};

const expectedTextEvent: TextEvent = {
	identifier: randomUUID(),
	value: randomBytes(256).toString(),
	taskId: randomUUID(),
	taskActions: [spamTaskAction],
};
const expectedArrayEvent: ArrayEvent = {
	identifier: randomUUID(),
	index: 0,
	value: randomUUID(),
	taskId: randomUUID(),
	taskActions: [spamTaskAction],
};
const expectedMapEvent: MapEvent = {
	identifier: randomUUID(),
	field: 'bio',
	value: randomBytes(256).toString(),
	taskId: randomUUID(),
	taskActions: [spamTaskAction],
};

const expectedAppealResult: WebhookEvent = {
	organization: randomUUID(),
	creator: undefined,
	content: undefined,
	appealResult: {
		decision: DecisionTypes.UPHELD,
		taskIds: [randomUUID()],
	} as AppealResult,
	// Events are the classifications. From system or human. If something is multiple violations there will be multiple events with the same location.
	// Since there are multiple sources for events. It can change. Content may be marked ok by a human. Or different systems disaggree in which case
	// Go with the most restrictive answer.
	events: [],
};

// eslint-disable-next-line max-lines-per-function
Tap.test('Streams Test', async (t: Tap.Test) => {
	await t.test('WebhookStreamLike in and out', async (t2: Tap.Test) => {
		let counter: number = 0;
		const stream: WebhookStreamLike<number> = new WebhookStreamLike();
		stream.listen((num: number) => {
			counter += num;
		});
		stream.emit(1);
		stream.emit(2);
		stream.emit(3);
		t2.same(counter, 6);
	});
	const stream: WebhookStreams = new WebhookStreams();
	await t.test('ContentStreams', async (t2: Tap.Test) => {
		const content: ContentStreams = stream.content;
		content.text.listen((event: TextEvent) => {
			t2.same(event.identifier, expectedTextEvent.identifier);
			t2.same(event.value, expectedTextEvent.value);
		});
		content.text.emit(expectedTextEvent);
		content.images.listen((event: ArrayEvent) => {
			t2.same(event.identifier, expectedArrayEvent.identifier);
			t2.same(event.index, expectedArrayEvent.index);
			t2.same(event.value, expectedArrayEvent.value);
		});
		content.images.emit(expectedArrayEvent);
		content.videos.listen((event: ArrayEvent) => {
			t2.same(event.identifier, expectedArrayEvent.identifier);
			t2.same(event.index, expectedArrayEvent.index);
			t2.same(event.value, expectedArrayEvent.value);
		});
		content.videos.emit(expectedArrayEvent);
		content.links.listen((event: ArrayEvent) => {
			t2.same(event.identifier, expectedArrayEvent.identifier);
			t2.same(event.index, expectedArrayEvent.index);
			t2.same(event.value, expectedArrayEvent.value);
		});
		content.links.emit(expectedArrayEvent);
		content.ipAddress.listen((event: TextEvent) => {
			t2.same(event.identifier, expectedTextEvent.identifier);
			t2.same(event.value, expectedTextEvent.value);
		});
		content.ipAddress.emit(expectedTextEvent);
	});
	await t.test('CreatorStreams', async (t2: Tap.Test) => {
		const creator: CreatorStreams = stream.creator;
		creator.uniqueName.listen((event: TextEvent) => {
			t2.same(event.identifier, expectedTextEvent.identifier);
			t2.same(event.value, expectedTextEvent.value);
		});
		creator.uniqueName.emit(expectedTextEvent);
		creator.images.listen((event: ArrayEvent) => {
			t2.same(event.identifier, expectedArrayEvent.identifier);
			t2.same(event.index, expectedArrayEvent.index);
			t2.same(event.value, expectedArrayEvent.value);
		});
		creator.images.emit(expectedArrayEvent);
		creator.links.listen((event: ArrayEvent) => {
			t2.same(event.identifier, expectedArrayEvent.identifier);
			t2.same(event.index, expectedArrayEvent.index);
			t2.same(event.value, expectedArrayEvent.value);
		});
		creator.links.emit(expectedArrayEvent);
		creator.moderatedFields.listen((event: MapEvent) => {
			t2.same(event.identifier, expectedMapEvent.identifier);
			t2.same(event.field, expectedMapEvent.field);
			t2.same(event.value, expectedMapEvent.value);
		});
		creator.moderatedFields.emit(expectedMapEvent);
	});
	await t.test('AppealStreams', async (t2: Tap.Test) => {
		const appeals: AppealStreams = stream.appeals;
		appeals.appealResult.listen((webhookEvent: WebhookEvent) => {
			t2.same(webhookEvent.appealResult?.decision, expectedAppealResult.appealResult?.decision);
			t2.same(webhookEvent.appealResult?.taskIds[0], expectedAppealResult.appealResult?.taskIds[0]);
		});
		appeals.appealResult.emit(expectedAppealResult);
	});
});
