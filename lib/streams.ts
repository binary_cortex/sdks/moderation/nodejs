import Debug from 'debug';
import WebhookEvent, { ClassificationState, TaskAction } from './objects/webhook.obj';

export type StreamCallback<T> = (t: T) => void | Promise<void>;

/** Does not actually use streams because node streams were designed while on LSD.
 * We are not going to use 3rd party streams either as I do not want to force the
 * client into having to use those same 3rd parties when we can write native code.
 */
export class WebhookStreamLike<T> {
	protected static debug: Debug.Debugger = Debug('BCModeration-streams');

	protected callbacks: StreamCallback<T>[] = [];

	emit(data: T): void {
		this.callbacks.forEach((fn: StreamCallback<T>) => fn(data));
	}

	listen(fn: (data: T) => void | Promise<void>): void {
		this.callbacks.push(fn);
	}
}

export interface TextEvent {
	identifier: string;

	value: string;

	taskId: string;

	taskActions: TaskAction[];
}

export interface ArrayEvent {
	identifier: string;

	index: number;

	value: string;

	taskId: string;

	taskActions: TaskAction[];
}

export interface MapEvent {
	identifier: string;

	field: string;

	value: string;

	taskId: string;

	taskActions: TaskAction[];
}

export interface ActionEvent {
	identifier: string;

	taskId: string;

	action: ClassificationState;
}

export type StreamEvent = TextEvent | MapEvent | ArrayEvent | ActionEvent;

export class ContentStreams {
	public context: WebhookStreamLike<ActionEvent> = new WebhookStreamLike<ActionEvent>();

	public text: WebhookStreamLike<TextEvent> = new WebhookStreamLike<TextEvent>();

	public images: WebhookStreamLike<ArrayEvent> = new WebhookStreamLike<ArrayEvent>();

	public videos: WebhookStreamLike<ArrayEvent> = new WebhookStreamLike<ArrayEvent>();

	public links: WebhookStreamLike<ArrayEvent> = new WebhookStreamLike<ArrayEvent>();

	public ipAddress: WebhookStreamLike<TextEvent> = new WebhookStreamLike<TextEvent>();
}

export class CreatorStreams {
	public context: WebhookStreamLike<ActionEvent> = new WebhookStreamLike<ActionEvent>();

	public uniqueName: WebhookStreamLike<TextEvent> = new WebhookStreamLike<TextEvent>();

	public images: WebhookStreamLike<ArrayEvent> = new WebhookStreamLike<ArrayEvent>();

	public links: WebhookStreamLike<ArrayEvent> = new WebhookStreamLike<ArrayEvent>();

	public moderatedFields: WebhookStreamLike<MapEvent> = new WebhookStreamLike<MapEvent>();
}

export class AppealStreams {
	public appealResult: WebhookStreamLike<WebhookEvent> = new WebhookStreamLike<WebhookEvent>();
}

export class WebhookStreams {
	public content: ContentStreams = new ContentStreams();

	public creator: CreatorStreams = new CreatorStreams();

	public appeals: AppealStreams = new AppealStreams();
}
