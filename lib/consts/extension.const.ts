export default class ExtensionConsts {
	static readonly EXT_TO_TYPE_MAP: Record<string, string> = {
		mp4: 'video/mp4',
		mpeg: 'video/mpeg',
		ts: 'video/mp2t',
		'3gp': 'video/3gpp',
		webm: 'video/webm',
		mov: 'video/quicktime',
		avi: 'video/x-msvideo',
		wmv: 'video/x-ms-wmv',
		ogv: 'video/ogg',
		jpeg: 'image/jpeg',
		jpg: 'image/jpeg',
		png: 'image/png',
		tiff: 'image/tiff',
		bmp: 'image/bmp',
		gif: 'image/gif',
		webp: 'image/webp',
	};
}
