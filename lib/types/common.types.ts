export type Constructor<T> = new ()=> T;

export type GenericRecord = Record<string, unknown>;

export type GenericMap = Map<string, unknown>;
