import Debug from 'debug';
import ExtensionConsts from './consts/extension.const';
import PresignedResponse from './objects/presigned-response.obj';
import { Stream } from 'stream';
import UnauthenticatedError from './errors/unauthenticated.error';
import {
	ActionEvent,
	ArrayEvent,
	ContentStreams,
	CreatorStreams,
	MapEvent,
	StreamEvent,
	TextEvent,
	WebhookStreams,
} from './streams';
import Appeal, { AppealResponse } from './objects/appeal.obj';
import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import Content, { PartialContent } from './objects/content.obj';
import Creator, { PartialCreator } from './objects/creator.obj';
import Crypto, { createVerify } from 'crypto';
import Report, { ReportResponse } from './objects/report.obj';
import WebhookEvent, {
	DataLocationObj,
	DataLocationType,
	Event,
	TaskAction,
} from './objects/webhook.obj';

export * from './consts/tos.enum';
export * from './errors/unauthenticated.error';
export * from './objects/content.obj';
export * from './objects/creator.obj';
export * from './objects/presigned-response.obj';
export * from './objects/report.obj';
export * from './objects/webhook.obj';
export * from './streams';

const USER_AGENT: string = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36';

export class BCModerationWebhookCore {
	protected axios: AxiosInstance;

	protected debug: Debug.Debugger = Debug('BCModeration');

	constructor(endpoint: string, keyId: string, keySecret: string) {
		this.axios = axios.create({
			baseURL: endpoint,
			timeout: 5000,
			headers: {
				'content-type': 'application/json',
				'user-agent': USER_AGENT,
			},
			auth: {
				username: keyId,
				password: keySecret,
			},
		});
	}

	public async post<DataType>(url: string, data: unknown): Promise<AxiosResponse<DataType, any>> | never {
		try {
			this.debug(`Making request to ${url}`);
			const response: AxiosResponse<DataType, any> = await this.axios.request<DataType>({
				method: 'post',
				url,
				data,
			});
			this.debug({
				status: response.status,
				data: response.data,
			});
			return response;
		} catch (error: any) {
			if (axios.isAxiosError(error)) {
				return this.handleAxiosError(error);
			}
			return this.handleUnexpectedError(error);
		}
	}

	protected handleAxiosError(error: AxiosError): never {
		this.debug('Error Config: %O', error.config);
		if (error.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			this.debug(error.response.data);
			this.debug(error.response.status);
			this.debug(error.response.headers);

			if (error.response.status === 401 || error.response.status === 403) {
				throw new UnauthenticatedError();
			}
		} else if (error.request) {
			// The request was made but no response was received
			// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
			// http.ClientRequest in node.js
			this.debug(error.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			this.debug('Error', error.message);
		}
		throw error;
	}

	protected handleUnexpectedError(error: unknown): never {
		this.debug('Error: %O', error);
		throw error;
	}
}

export class BCModerationWebhookUtil {
	protected static readonly SIGNATURE_ALGORITHM: string = 'RSA-SHA512';

	protected static readonly SIGNATURE_ENCODING: Crypto.BinaryToTextEncoding = 'base64';

	protected verificationKey: Buffer | undefined;

	protected core: BCModerationWebhookCore;

	constructor(core: BCModerationWebhookCore) {
		this.core = core;
	}

	protected async getVerificationKey(): Promise<Buffer> {
		if (!this.verificationKey) {
			const response: AxiosResponse<{ key: string }> = await this.core.post('/v1/developer/webhook/verify-key', { placeholder: true });
			this.verificationKey = Buffer.from(response.data.key, 'base64');
		}
		return this.verificationKey;
	}

	public async verifySignature(signature: string, webhookBody: string): Promise<boolean> {
		const verify: Crypto.Verify = createVerify(BCModerationWebhookUtil.SIGNATURE_ALGORITHM);
		verify.update(webhookBody);
		verify.end();
		const publicKey: Crypto.KeyObject = Crypto.createPublicKey({
			key: await this.getVerificationKey(),
			format: 'der',
			type: 'spki',
		});
		return verify.verify(publicKey, signature, BCModerationWebhookUtil.SIGNATURE_ENCODING);
	}
}

export default class BCModeration {
	public static readonly SIGNATURE_HEADER: string = 'x-signature';

	public streams: WebhookStreams = new WebhookStreams();

	protected core: BCModerationWebhookCore;

	protected webhookUtil: BCModerationWebhookUtil;

	protected debug: Debug.Debugger = Debug('BCModeration');

	protected verificationKey: Buffer | undefined;

	// @todo Fill in production endpoint here
	constructor(keyId: string, keySecret: string, endpoint: string = 'https://') {
		this.core = new BCModerationWebhookCore(endpoint, keyId, keySecret);
		this.webhookUtil = new BCModerationWebhookUtil(this.core);
	}

	// @todo Consider some kind of restriction on file type
	async getPresignedForMediaUpload(identifier: string, fileType: string): Promise<PresignedResponse> {
		const response: AxiosResponse<PresignedResponse> = await this.core.post('/v1/submit/presigned', { identifier, fileType });
		return response.data;
	}

	public async submitMedia(
		identifier: string,
		fileType: string,
		data: Buffer | ArrayBuffer | ArrayBufferView | Stream,
	): Promise<void> {
		const { url }: PresignedResponse = await this.getPresignedForMediaUpload(identifier, fileType);
		try {
			await axios.put(url, data, {
				headers: {
					'Content-Type': ExtensionConsts.EXT_TO_TYPE_MAP[fileType],
					'User-Agent': USER_AGENT,
				},
			});
		} catch (error) {
			// @todo Handle errors here.
		}
	}

	public async reportContent(
		contentIdentifier: string,
		reporter: string,
		reason: string,
		location?: DataLocationObj,
	): Promise<ReportResponse> {
		const report: Report = {
			content: contentIdentifier,
			reporter,
			reason,
			location,
		};
		const response: AxiosResponse<ReportResponse> = await this.core.post('/v1/submit/report', report);
		return response.data;
	}

	public async reportCreator(
		creatorIdentifier: string,
		reporter: string,
		reason: string,
		location?: DataLocationObj,
	): Promise<ReportResponse> {
		const report: Report = {
			creator: creatorIdentifier,
			reporter,
			reason,
			location,
		};
		const response: AxiosResponse<ReportResponse> = await this.core.post('/v1/submit/report', report);
		return response.data;
	}

	public async appeal(taskId: string): Promise<AppealResponse> {
		const appeal: Appeal = {
			taskId,
		};
		const response: AxiosResponse<AppealResponse> = await this.core.post('/v1/submit/appeal', appeal);
		return response.data;
	}

	public async verifySignature(signature: string, webhookBody: string): Promise<boolean> {
		return this.webhookUtil.verifySignature(signature, webhookBody);
	}

	protected sendToCreatorStream(locationName: string | undefined, data: StreamEvent): void {
		const streams: CreatorStreams = this.streams.creator;
		switch (locationName) {
			case undefined:
				streams.context.emit(data as ActionEvent);
				break;
			case 'uniqueName':
				streams.uniqueName.emit(data as TextEvent);
				break;
			case 'imageIds':
				streams.images.emit(data as ArrayEvent);
				break;
			case 'links':
				streams.links.emit(data as ArrayEvent);
				break;
			case 'moderatedFields':
				streams.moderatedFields.emit(data as MapEvent);
				break;
			default:
				break;
		}
	}

	protected sendToContentStream(locationName: string | undefined, data: StreamEvent): void {
		const streams: ContentStreams = this.streams.content;
		switch (locationName) {
			case undefined:
				streams.context.emit(data as ActionEvent);
				break;
			case 'text':
				streams.text.emit(data as TextEvent);
				break;
			case 'imageIds':
				streams.images.emit(data as ArrayEvent);
				break;
			case 'videoIds':
				streams.videos.emit(data as ArrayEvent);
				break;
			case 'links':
				streams.links.emit(data as ArrayEvent);
				break;
			case 'ipAddress':
				streams.ipAddress.emit(data as TextEvent);
				break;
			default:
				break;
		}
	}

	// eslint-disable-next-line max-lines-per-function
	protected buildStreamEvent(
		location: DataLocationObj,
		identifier: string,
		value: string,
		taskId: string,
		taskActions: TaskAction[],
	): StreamEvent | undefined {
		let data: StreamEvent | undefined;
		switch (location.type) {
			case DataLocationType.FIELD:
				data = {
					identifier,
					value,
					taskId,
					taskActions,
				};
				break;
			case DataLocationType.ARRAY:
				data = {
					identifier,
					index: location.value as number,
					value,
					taskId,
					taskActions,
				};
				break;
			case DataLocationType.OBJECT:
				data = {
					identifier,
					field: (location.value as DataLocationObj).name,
					value,
					taskId,
					taskActions,
				};
				break;
			default:
				// Bad data
				break;
		}
		return data;
	}

	// eslint-disable-next-line max-lines-per-function
	protected async distributeStreamEvents(event: Event, isCreator: boolean): Promise<void> {
		const {
			identifier, value, taskId, taskActions, action,
		}: Event = event;
		if (typeof action !== 'undefined') {
			const data: ActionEvent = {
				identifier,
				taskId,
				action,
			};
			if (data) {
				if (isCreator) {
					this.sendToCreatorStream(undefined, data);
				} else {
					this.sendToContentStream(undefined, data);
				}
			}
			return;
		}
		// @todo Send some indication of context through the streams
		event.locations.forEach((location: DataLocationObj) => {
			const data: StreamEvent | undefined = this.buildStreamEvent(
				location,
				identifier,
				value,
				taskId,
				taskActions,
			);
			if (data) {
				if (isCreator) {
					this.sendToCreatorStream(location.name, data);
				} else {
					this.sendToContentStream(location.name, data);
				}
			}
		});
	}

	/** Processes and verifies the webhook and converts it out an event on one of the streams */
	public async processWebhook(webhookBody: WebhookEvent, signature: string): Promise<boolean> {
		if (!await this.verifySignature(signature, JSON.stringify(webhookBody))) {
			// Verification failed.
			return false;
		}

		const isAppeal: boolean = typeof webhookBody.appealResult !== 'undefined';
		if (isAppeal) {
			this.streams.appeals.appealResult.emit(webhookBody);
			return true;
		}
		const isCreator: boolean = typeof webhookBody.creator !== 'undefined';
		const identifier: string = webhookBody.creator ?? webhookBody.content ?? '';
		if (!identifier) {
			return false;
		}

		webhookBody.events.forEach((event: Event) => this.distributeStreamEvents(event, isCreator));
		return true;
	}

	public async submitCreator(creator: Creator | PartialCreator): Promise<void> {
		await this.core.post('/v1/submit', { creator });
	}

	public async submitContent(content: Content | PartialContent): Promise<void> {
		await this.core.post('/v1/submit', { content });
	}

	/** Delete content and related assets */
	public async deleteContent(content: Content | PartialContent): Promise<void> {
		await this.core.post('/v1/submit/delete', { contentIdentifier: content.contentIdentifier });
	}

	/** return JSON.parse(webhookBody) as WebhookEvent */
	public parseWebhookBody(webhookBody: string): WebhookEvent {
		return JSON.parse(webhookBody) as WebhookEvent;
	}
}
