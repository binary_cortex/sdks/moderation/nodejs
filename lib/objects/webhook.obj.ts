import TOS, { NonTOS } from '../consts/tos.enum';

export enum DataLocationType {
	FIELD = 1,
	OBJECT = 2,
	ARRAY = 3,
}

export enum ActionResult {
	NONE = 0,
	NOACTION = 1,
	REMOVAL = 2,
	NFSW = 3,
}

export enum ClassificationState {
	BRAND_SAFE = 'brand_safe',
	APPROVED = 'approved',
	REVIEW = 'review',
	REMOVE = 'remove',
}

export enum AccountAction {
	NONE = 0,
	REMOVAL = 1,
}

export interface TaskAction {
	id: string;
	name: string;
	description: string;
	image: string;
	childIds: string[];
	organizationId: string | null;
	root: boolean;
	actionResult: ActionResult;
	appealable: boolean | null;
	slug: TOS | NonTOS;
	accountAction: AccountAction;
}

export type DataLocationValue = DataLocationObj | number;

export interface DataLocationObj {
	type: DataLocationType
	name: string;
	value?: DataLocationValue; // Only on object and array
}

export interface Event {
	identifier: string;
	// The location of the classification such as uniqueName, moderatedFields.bio, images[0]. Can be more than one if context matters.
	locations: DataLocationObj[];
	taskId: string;
	taskActions: TaskAction[];
	// The content. Text, imageId, link, etc.
	value: string;
	action?: ClassificationState;
}

export enum DecisionTypes {
	UPHELD = 'upheld',
	OVERTURNED = 'overturned',
}

export interface AppealResult {
	decision: DecisionTypes;
	taskIds: string[];
}

interface WebhookEvent {
	organization: string;
	creator?: string;
	content?: string;
	appealResult?: AppealResult;
	// Events are the classifications. From system or human. If something is multiple violations there will be multiple events with the same location.
	// Since there are multiple sources for events. It can change. Content may be marked ok by a human. Or different systems disaggree in which case
	// Go with the most restrictive answer.
	events: Event[];
}

export default WebhookEvent;
