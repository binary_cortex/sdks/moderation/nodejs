interface Content {
	created: string;
	contentType: string;
	creatorIdentifier: string;
	contentIdentifier: string;
	parentIdentifier?: string;
	text?: string;
	imageIds?: string[];
	videoIds?: string[];
	links?: string[];
	metadata?: Record<string, unknown>;
	ipAddress?: string;
	publicUrl?: string;
}

export type PartialContent = Partial<Content> & {
	contentIdentifier: string,
}

export default Content;
