import { DataLocationObj } from './webhook.obj';

interface Report {
	content?: string;
	creator?: string;
	reporter: string;
	reason: string;
	location?: DataLocationObj;
}

export enum ReportErrors {
	REPORTER_NOT_SUBMITTED = 1,
	CREATOR_NOT_SUBMITTED = 2,
	CONTENT_NOT_SUBMITTED = 3,
}

export interface ReportResponse {
	error: boolean;
	code?: ReportErrors;
	message: string;
}

export default Report;
