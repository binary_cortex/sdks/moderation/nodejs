export interface PresignedResponse {
	url: string;
}

export default PresignedResponse;
