interface Creator {
	creatorIdentifier: string;
	created: string;
	uniqueName: string;
	imageIds: string[];
	links?: string[];
	moderatedFields: Record<string, unknown>;
	metadata?: Record<string, unknown>;
	publicUrl?: string;
}

export type PartialCreator = Partial<Creator> & {
	creatorIdentifier: string,
}

export default Creator;
