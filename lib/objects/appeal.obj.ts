export enum AppealErrors {
  MISSING_TASK_ID = 1,
  NO_TASK = 2,
  NO_HUMAN_REVIEWS = 3,
  NO_VIOLATION = 4,
  NO_ACTION = 5,
  NO_REQUEST = 6,
  NO_CONTEXT = 7,
  NOT_APPEALABLE = 8,
  IN_APPEAL = 9,
  APPEAL_ALREADY_PROCESSED = 10,
}

interface Appeal {
  taskId: string;
}

export interface AppealResponse {
  error: boolean;
  code?: AppealErrors;
  message: string;
}

export default Appeal;
